#!/usr/bin/env bash
#
# Required global variables
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_REGION
#   APP_ID
#
# Optional global variables
#   ENVIROMENT_NAME (default: "prod")
#   ZIP_FILE_ROUTE (default: ./build.zip)
#

# Required parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID environment variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_ACCESS_KEY_ID environment variable missing.'}
AWS_REGION=${AWS_REGION:?'AWS_REGION environment variable missing.'}
APP_ID=${APP_ID:?'APP_ID environment variable missing.'}

# Default parameters
ENVIROMENT_NAME=${ENVIROMENT_NAME:="prod"}
ZIP_FILE_ROUTE=${ZIP_FILE_ROUTE:="./build.zip"}

# Configuring AWS CLI
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set region $AWS_REGION

# Create Deployment on Amplify
RESULT=$(aws amplify create-deployment --app-id $APP_ID --branch-name $ENVIROMENT_NAME)
JOB_ID=$(echo "$RESULT" | jq -r '.jobId')
ZIP_UPLOAD_URL=$(echo "$RESULT" | jq -r '.zipUploadUrl')

# Upload Deployment File
curl $ZIP_UPLOAD_URL --upload-file $ZIP_FILE_ROUTE

#Start Deployment
RESULT=$(aws amplify start-deployment --app-id $APP_ID --branch-name $ENVIROMENT_NAME --job-id $JOB_ID)
STATUS=$(echo "$RESULT" | jq -r '.jobSummary.status')

# Wait Deployment Result
while [ "$STATUS" != "SUCCEED" ] && [ "$STATUS" != "FAILED" ]; do
  RESULT=$(aws amplify get-job --app-id $APP_ID --branch-name $ENVIROMENT_NAME --job-id $JOB_ID)
  STATUS=$(echo "$RESULT" | jq -r '.job.summary.status')
  sleep 3
done

if [ "$STATUS" == "FAILED" ]; then
  exit 1
fi
exit 0
