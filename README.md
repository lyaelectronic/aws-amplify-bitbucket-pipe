# Bitbucket Pipelines Pipe: AWS Amplify Deploy

Deploy to AWS Amplify

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: lyaelectronic/aws-amplify-bitbucket-pipe:latest
    variables:
      AWS_ACCESS_KEY_ID: "<string>"
      AWS_SECRET_ACCESS_KEY: "<string>"
      AWS_REGION: "<string>"
      APP_ID: "<string>"
      # ENVIROMENT_NAME: "<string>" # Optional
      # ZIP_FILE_ROUTE: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable                  | Usage                                                             |
| ------------------------- | ----------------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*)     | AWS IAM access key ID. |
| AWS_SECRET_ACCESS_KEY(*)  | AWS IAM secret access key. |
| AWS_REGION (*)            | AWS Amplify app region. |
| APP_ID (*)                | AWS Amplify app ID. |
| ENVIROMENT_NAME           | Deploy enviroment/branch for your app. Default: `prod`. |
| ZIP_FILE_ROUTE            | Path and name of your app build zip file. Default: `./build.zip`. |
| DEBUG                     | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

This pipe allows to create a deployment for a manually deployed Amplify app in Bitbucket pipelines. Keep in mind that manually deployed apps are not connected to a repository.

## Examples

### Basic example:

Deploy app to amplify.
```yaml
script:
  - pipe: lyaelectronic/aws-amplify-bitbucket-pipe:latest
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_REGION: $AWS_REGION
      APP_ID: $APP_ID
```

### Advanced example:

Example with aditional arguments to specify .zip file name and path, using a branch diferent to prod.
```yaml
script:
  - pipe: lyaelectronic/aws-amplify-bitbucket-pipe:latest
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_REGION: $AWS_REGION
      APP_ID: $APP_ID
      ENVIROMENT_NAME: "production"
      ZIP_FILE_ROUTE: "./web.zip"
      DEBUG: "true"
```

## Support

If you’d like help with the development of this pipe, or if something happened to you while using it, let us know.
Forward an email to info@lya-electronic.com

If you’re reporting an problem, please also include:

- steps to reproduce
- the version of the pipe
- relevant logs and error messages

## License

Copyright (c) 2020 LYA Electronic CORP and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.